package thinkback.actividad1.model;


public class Perfil {
    private String nombre;
    private String email;
    private String tlf;
    private String direccion;
    private int dia;
    private int mes;
    private int year;

    public Perfil(){
        this.nombre = "";
        this.email = "";
        this.tlf = "";
        this.direccion = "";
        this.dia = -1;
        this.mes = -1;
        this.year = -1;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTlf() {
        return tlf;
    }

    public void setTlf(String tlf) {
        this.tlf = tlf;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
