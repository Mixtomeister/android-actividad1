package thinkback.actividad1.model.libro;

import java.util.ArrayList;

public class Libro {

    private ArrayList<Pagina> paginas;
    private int pag;
    private boolean first;
    private boolean last;

    public Libro(){
        this.paginas = new ArrayList<Pagina>();
        this.pag = -1;
    }

    public Pagina getActPag(){
        if(this.pag == 0){
            this.first = true;
        }else if(this.pag == (this.paginas.size() - 1)){
            this.last = true;
        }else{
            this.first = false;
            this.last = false;
        }
        return this.paginas.get(this.pag);
    }

    public void nextPag(){
        this.pag++;
    }

    public void prevPag(){
        this.pag++;
    }

    public boolean isFirst() {
        return first;
    }

    public boolean isLast() {
        return last;
    }

    public int getPag(){
        return this.pag;
    }

    public void addPagina(Pagina pag){
        this.paginas.add(pag);
    }
}
