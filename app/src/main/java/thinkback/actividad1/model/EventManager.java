package thinkback.actividad1.model;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class EventManager implements View.OnClickListener{

    private AppCompatActivity activity;

    public EventManager(AppCompatActivity activity){
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {}
}