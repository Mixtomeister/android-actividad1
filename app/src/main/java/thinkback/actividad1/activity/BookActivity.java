package thinkback.actividad1.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import thinkback.actividad1.model.DataManager;
import thinkback.actividad1.model.EventManager;
import thinkback.actividad1.R;
import thinkback.actividad1.model.libro.Libro;
import thinkback.actividad1.model.libro.Pagina;

public class BookActivity extends AppCompatActivity {

    private EventManager events;

    private Button btn_perfil;
    private Button btn_ant;
    private Button btn_sig;

    private EditText txt_titulo;
    private EditText txt_contenido;

    private Libro libro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        this.libro = DataManager.getSharedInstance().getLibro();

        this.events = new EventManager(this){
            @Override
            public void onClick(View view) {
                clickEvent(view);
            }
        };

        btn_perfil = findViewById(R.id.btn_perfil);
        btn_ant = findViewById(R.id.btn_ant);
        btn_sig = findViewById(R.id.btn_sig);
        txt_titulo = findViewById(R.id.txt_titulo);
        txt_contenido = findViewById(R.id.txt_contenido);

        btn_perfil.setOnClickListener(events);
        btn_ant.setOnClickListener(events);
        btn_sig.setOnClickListener(events);

        cargarPagina();
    }

    private void clickEvent(View view){
        if(view.getId() == R.id.btn_perfil){
            finish();
        }else if(view.getId() == R.id.btn_ant){
            this.libro.prevPag();
            cargarPagina();
        }else if(view.getId() == R.id.btn_sig){
            this.libro.nextPag();
            cargarPagina();
        }
    }

    public void cargarPagina(){
        Pagina pag = this.libro.getActPag();
        if(this.libro.isFirst()){
            btn_ant.setEnabled(false);
            btn_sig.setEnabled(true);
        }else if (this.libro.isLast()){
            btn_ant.setEnabled(true);
            btn_sig.setEnabled(false);
        }else{
            btn_ant.setEnabled(true);
            btn_sig.setEnabled(true);
        }
        txt_titulo.setText(pag.getTitulo());
        txt_contenido.setText(pag.getContenido());
    }
}
