package thinkback.actividad1.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import thinkback.actividad1.model.DataManager;
import thinkback.actividad1.model.EventManager;
import thinkback.actividad1.model.Perfil;
import thinkback.actividad1.R;

public class MainActivity extends AppCompatActivity {

    private EventManager events;
    private boolean editable;

    //Declaración de Views
    private Button btn_sig_can;
    private Button btn_edit_save;

    private EditText txt_name;
    private EditText txt_email;
    private EditText txt_tlf;
    private EditText txt_dir;

    private DatePicker dt_fecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        events = new EventManager(this){
            @Override
            public void onClick(View view) {
                clickEvent(view);
            }
        };

        //Implementación de Views
        btn_edit_save = findViewById(R.id.btn_edit_save);
        btn_sig_can = findViewById(R.id.btn_sig_can);

        txt_name = findViewById(R.id.txt_name);
        txt_email = findViewById(R.id.txt_email);
        txt_tlf = findViewById(R.id.txt_tlf);
        txt_dir = findViewById(R.id.txt_dir);

        dt_fecha = findViewById(R.id.dt_fecha);

        btn_edit_save.setOnClickListener(events);
        btn_sig_can.setOnClickListener(events);

        this.cargarDatos();

        this.setNotEditable();
    }

    public void setNotEditable(){
        editable = false;
        btn_sig_can.setText(R.string.btn_book);
        btn_edit_save.setText(R.string.btn_edit);
        txt_name.setEnabled(false);
        txt_email.setEnabled(false);
        txt_tlf.setEnabled(false);
        txt_dir.setEnabled(false);
        dt_fecha.setEnabled(false);
        txt_name.setFocusable(false);
        txt_email.setFocusable(false);
        txt_tlf.setFocusable(false);
        txt_dir.setFocusable(false);
        txt_name.setFocusableInTouchMode(false);
        txt_email.setFocusableInTouchMode(false);
        txt_tlf.setFocusableInTouchMode(false);
        txt_dir.setFocusableInTouchMode(false);
    }

    public void setEditable(){
        editable = true;
        btn_sig_can.setText(R.string.btn_can);
        btn_edit_save.setText(R.string.btn_save);
        txt_name.setEnabled(true);
        txt_email.setEnabled(true);
        txt_tlf.setEnabled(true);
        txt_dir.setEnabled(true);
        dt_fecha.setEnabled(true);
        txt_name.setFocusable(true);
        txt_email.setFocusable(true);
        txt_tlf.setFocusable(true);
        txt_dir.setFocusable(true);
        txt_name.setFocusableInTouchMode(true);
        txt_email.setFocusableInTouchMode(true);
        txt_tlf.setFocusableInTouchMode(true);
        txt_dir.setFocusableInTouchMode(true);
    }

    public void guardarDatos(){
        Perfil perfil = DataManager.getSharedInstance().getPerfil();
        perfil.setNombre(txt_name.getText().toString());
        perfil.setEmail(txt_email.getText().toString());
        perfil.setTlf(txt_tlf.getText().toString());
        perfil.setDireccion(txt_dir.getText().toString());
        perfil.setDia(dt_fecha.getDayOfMonth());
        perfil.setMes(dt_fecha.getMonth());
        perfil.setYear(dt_fecha.getYear());
    }

    public void cargarDatos(){
        Perfil perfil = DataManager.getSharedInstance().getPerfil();
        txt_name.setText(perfil.getNombre());
        txt_email.setText(perfil.getEmail());
        txt_tlf.setText(perfil.getTlf());
        txt_dir.setText(perfil.getDireccion());
        if(perfil.getDia() != -1)
            dt_fecha.updateDate(perfil.getYear(), perfil.getMes(), perfil.getDia());
    }

    private void clickEvent(View view){
        if(view.getId() == R.id.btn_edit_save){
            if(isEditable()){
                setNotEditable();
                guardarDatos();
            }else{
                setEditable();
            }
        }else if(view.getId() == R.id.btn_sig_can){
            if(isEditable()){
                setNotEditable();
            }else{
                Intent intent = new Intent(this, BookActivity.class);
                startActivity(intent);
            }
        }
    }

    public boolean isEditable(){
        return this.editable;
    }
}
